#JADER HENRIQUE FARIA AMORIM	

### PROJETO PROGRAMA��O COM SOCKETS.


Fa�a um dos exerc�cios seguintes:

Exerc�cio 1: Fa�a um servidor que receba uma string e retorne o resultado invertido, por exemplo: recebe 'amor' e retorna 'roma'.

Exerc�cio 2: Fa�a um servidor que recebe uma mensagem com uma express�o aritm�tica, e retorne o resultado, por exemplo: envia a mensagem '2+3*4' e retorna '14'. Dica: utilize a fun��o 'eval' para analisar a express�o.

Exerc�cio 3: Fa�a um servidor que receba uma string UDP e retorne de volta a string em mai�sculo.

Exerc�cio 4: Fa�a um servidor que receba uma string e imprima a string na base hexadecimal.

Exerc�cio 5: Fa�a um cliente que envie a string 'ola!' em hexadecimal e um servidor que receba e imprima a string recebida.

Exerc�cio 6: Desenvolva um cliente e um servidor que transfira um arquivo para o outro.

Exerc�cio 7: Implemente um servidor HTTP b�sico, utilizando os recursos de sockets puros, sem usar a classe 'http'. O servidor deve servir basicamente um ou mais arquivos HTML e retornar um c�digo de erro adequado quando o arquivo n�o existir.

Exerc�cio 8: Fa�a um c�digo cliente que conecte e entre em um jogo de um servidor tetrinet. Dica: utilize o software wireshark para analisar o protocolo.
